# Samply MDR GUI

Samply MDR is an implementation of ISO 11179-3. It offers various REST
interfaces, a convenient graphical userinterface, an adapter to other
MDR implementations like the caDSR MDR, and other great features.

This project implements the Graphical User Interface *only*.

# IMPORTANT

As of September 1st, 2017, the repository has moved to https://bitbucket.org/medicalinformatics/samply.mdr.gui. Please see the following help article on how to change the repository location in your working copies:

    https://help.github.com/articles/changing-a-remote-s-url/

If you have forked Samply.MDR in Bitbucket, the fork is now linked to the new location automatically. Still, you should change the location in your local copies (usually, the origin of the fork is configured as a remote with name "upstream" when cloning from Bitbucket).